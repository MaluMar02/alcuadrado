#include <stdio.h>

int main(void){

	int numero; // Declaro variable `numero`
	int resultado; 

	printf("%s", "Ingrese número:");
	scanf("%d", &numero);

	resultado = numero * numero;

	printf("La segunda potencia de %d es %d.\n", numero, resultado);

}
