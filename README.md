# alCuadrado

Escribe un programa que pregunte un número entero al usuario, lo obtenga
y retorne el número al cuadrado.

## Refinamiento 1

1. Pregunto el número al usuario
1. Obtengo el número
1. Elevo el número al cuadrado
1. Imprimo el resultado

## Refinamiento 2

1. Declaro variable `numero`
1. Declaro variable `resultado`
1. Pregunto el número al usuario
1. Obtengo el número y asigno a variable `numero`
1. Calculo el cuadrado de `numero` y asigno a `resultado`
1. Imprimir `resultado` en pantalla.
